# Validator Service

check a string against a list of rules, if one of them failed, it fails.

## Context

- [Validator Service](#validator-service)
  - [Context](#context)
  - [Expected Usage](#expected-usage)
    - [With callback](#with-callback)
    - [Without callback](#without-callback)
    - [Adding a new rule](#adding-a-new-rule)

## Expected Usage

### With callback

```dart
List<String> _ruleList_ = ['hasData', 'isEmail', 'isPassword'];

ValidatorService.check(
    'foo@bar.com',
    _ruleList,
    onError: (ValidatorResponse res) => print(res.errors),
    onSuccess: (ValidatorResponse res) => print('Success'),
);
```

### Without callback

```dart
List<String> _ruleList_ = ['hasData', 'isEmail', 'isPassword'];
ValidatorResponse _response;

_response = ValidatorService.check(
    'PaSwOwdL123',
    _ruleList,
);
```

> (Optional) `(int) minChars` argument to set a minimum number of characters before validating
> (Optional) `(bool) debug` argument to print logs

### Adding a new rule

- go to [validator_rules.dart](https://bitbucket.org/wzac-team-repo/package-validator-service/src/master/lib/validator_rules.dart)
- add a new `ValidatorRule` class to list

```dart
ValidatorRule(
    name: 'isEqualsToOne',
    f: (value) {
        return int.parse(value) == 1;
    },
    message: 'Value has to be one',
),
```
