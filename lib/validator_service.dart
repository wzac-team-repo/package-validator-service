import 'package:flutter/services.dart';

import 'models/validator_response.dart';
import 'models/validator_result.dart';
import 'models/validator_rule.dart';
import 'validator_rules.dart';

class ValidatorService {
  static bool isDebugging;

  /**
   * returns a { ValidatorResponse } 
   * @param { String } value is the value that you want to validate
   * @param { List } a list of validator name where each string is represented a single method within this class
   * @param { Function } callback return a ValidatorResponse class where you can get its { status } and { list } 
   * Calculate the total number of rules to check against vs. the number or rules passed
   */
  static ValidatorResponse check(
    String value,
    List<String> validatorNames, {
    onSuccess(ValidatorResponse response),
    onError(ValidatorResponse response),
    int minChars = 0,
    debug = false,
  }) {
    if (value.length <= minChars) return null;

    isDebugging = debug;

    _debug("========== ValidatorService ==========");
    ValidatorResponse _response;
    List<ValidatorResult> rules = _ruleList(value, validatorNames);
    int totalRules = validatorNames.length;
    int totalPassed = rules.where((vr) => vr.result == true).length;

    // === errors ===
    if (totalPassed < totalRules) {
      _response = _responseError(rules);

      if (onError.isNotNull) {
        onError(_response);
      }
      return _response;
    }

    // === success ===
    _response = _responseSuccess(rules);
    if (onSuccess.isNotNull) {
      onSuccess(_response);
    }
    return _response;
  }

  /**
   * (Private) if response has error
   * @param { List } rules
   * return ValidatorResponse{
   *  { String } status => 'success' or 'error'
   *  { List<ValidatorResult> } list => a list of rules that have been failed
   *  { List<String> } errors => a list of error messages
   * }
   */
  static ValidatorResponse _responseError(List<ValidatorResult> rules) {
    _debug("❌  Failed");

    List<ValidatorResult> _newList;
    List<String> _errors;
    _newList = rules.where((vr) => vr.result == false).toList();
    _errors = _newList.map((item) => item.errorMessage).toList();

    if (isDebugging) {
      _errors.map((error) => _debug("⚠ $error")).toList();
    }

    return ValidatorResponse(status: 'error', list: _newList, errors: _errors);
  }

  /**
   * (Private) is response is successful
   * @param { List } rules
   * return ValidatorResponse{
   *  { String } status => 'success' or 'error'
   *  { List<ValidatorResult> } list => a list of rules that have been sucessful
   *  { List<String> } errors => null (no errors)
   * }
   */
  static ValidatorResponse _responseSuccess(List<ValidatorResult> rules) {
    _debug("✅  Passed");

    List<ValidatorResult> _newList;
    _newList = rules.where((vr) => vr.result == true).toList();

    return ValidatorResponse(status: 'success', list: _newList, errors: null);
  }

  /**
   * (Private) a list of validations to go through
   * @param { String } value to check against
   */
  static List<ValidatorResult> _ruleList(value, list) {
    List<ValidatorResult> _results = [];

    for (int i = 0; i < list.length; i++) {
      String _ruleName = list[i];
      ValidatorRule _rule =
          ValidatorRules.rules.firstWhere((rule) => rule.name == _ruleName);

      _results.add(
        ValidatorResult(
          _rule.name,
          _rule.f(value),
          _rule.message,
        ),
      );
    }

    return _results;
  }

  /**
   * ========== Debug ==========
   */
  static void _debug(value) {
    if (isDebugging) print(value);
  }
}

extension ValidatorStringExtension on Object {
  bool get isNull => this == null;
  bool get isNotNull => this != null;
}
