import 'models/validator_rule.dart';

class ValidatorRules {
  static List<ValidatorRule> rules = [
    /**
   * Check if value has data
   */
    ValidatorRule(
      name: 'hasData',
      f: (value) {
        return value != null && value.length > 0 && !value.contains(' ');
      },
      message: 'Value cannot be empty',
    ),

    /**
   * Check if value is email
   * Reference: https://stackoverflow.com/a/50663835/6212975
   */
    ValidatorRule(
      name: 'isEmail',
      f: (value) {
        return value != null &&
            RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                .hasMatch(value);
      },
      message: 'Email address is not valid',
    ),

    /**
   * Check if is password
   * Reference: https://stackoverflow.com/questions/56253787/how-to-handle-textfield-validation-in-password-in-flutter/56256456
   * Minimum 1 Upper case
   * Minimum 1 lowercase
   * Minimum 1 Numeric Number
   * Minimum 1 Special Character
   * Common Allow Character ( ! @ # $ & * ~ )
   */
    ValidatorRule(
      name: 'isPassword',
      f: (value) {
        String pattern =
            r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
        RegExp regExp = new RegExp(pattern);
        return value != null && regExp.hasMatch(value);
      },
      message: 'Password is not valid',
    ),
  ];
}
