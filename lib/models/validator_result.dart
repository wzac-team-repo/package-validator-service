class ValidatorResult {
  final String functionName;
  final bool result;
  final String errorMessage;

  ValidatorResult(this.functionName, this.result, this.errorMessage);
}
