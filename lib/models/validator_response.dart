import 'validator_result.dart';

class ValidatorResponse {
  final String status;
  final List<ValidatorResult> list;
  final List<String> errors;

  ValidatorResponse({this.status, this.list, this.errors});
}
