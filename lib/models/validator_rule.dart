class ValidatorRule {
  final String name;
  final Function f;
  final String message;

  ValidatorRule({this.name, this.f, this.message});
}
