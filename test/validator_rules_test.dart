import 'package:flutter_test/flutter_test.dart';
import '../lib/validator_rules.dart';
import '../lib/models/validator_rule.dart';

ValidatorRule getRule(String ruleName) {
  return ValidatorRules.rules.firstWhere((rule) => rule.name == ruleName);
}

void main() {
  group('[hasData]::', () {
    final ValidatorRule _rule = getRule('hasData');

    test('filled string should return true', () {
      final String _value = '123';

      expect(_rule.f(_value), true);
    });

    test('empty string should return false', () {
      final String _value = '';

      expect(_rule.f(_value), false);
    });

    test('string contains space should return false', () {
      final String _value = ' ';

      expect(_rule.f(_value), false);
    });
  });
}
